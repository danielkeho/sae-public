# recordsManagement/messages
# Language: fr
# Content-Type: text/plain; charset=UTF-8

msgid "recordsManagement"
msgstr "Archives"
msgctxt "bundle"

msgid "accessRule"
msgstr "Gestion des codes de communicabilité"
msgctxt "interface"

msgid "archivalAgreement"
msgstr "Gestion des conventions d'archivage"
msgctxt "interface"

msgid "archivalProfile"
msgstr "Gestion des profils d'archivage"
msgctxt "interface"

msgid "archiveRelationship"
msgstr "Gestion de relations entre archives"
msgctxt "interface"

msgid "deposit"
msgstr "Dépôt d'archives"
msgctxt "interface"

msgid "lifeCycleJournal"
msgstr "Consultation du journal de cycle de vie"
msgctxt "interface"

msgid "log"
msgstr "Consultation du journal"
msgctxt "interface"

msgid "modify"
msgstr "Modification d'archives"
msgctxt "interface"

msgid "retrieve"
msgstr "Recherche d'archives"
msgctxt "interface"

msgid "serviceLevel"
msgstr "Gestion des niveaux de service"
msgctxt "interface"

msgid "retentionRule"
msgstr "Gestion des règles de conservation"
msgctxt "interface"

msgid "message"
msgstr "Gestion de bordereaux"
msgctxt "interface"

msgid "document"
msgstr "Document"
msgctxt "model"

msgid "accessRule"
msgstr "Règles de communicabilité"
msgctxt "model"

msgid "archivalAgreement"
msgstr "Conventions d'archivage"
msgctxt "model"

msgid "archivalProfile"
msgstr "Profils d'archivage"
msgctxt "model"

msgid "disposalDate"
msgstr "Date d'élimination"
msgctxt "model"

msgid "archive"
msgstr "Archives"
msgctxt "model"

msgid "archiveRelationship"
msgstr "Relations entre archives"
msgctxt "model"

msgid "archiveRelationshipType"
msgstr "Types de relations entre archives"
msgctxt "model"

msgid "disposalRule"
msgstr "Règles d'éliminations"
msgctxt "model"

msgid "log"
msgstr "Logs"
msgctxt "model"

msgid "archiveDescription"
msgstr "Description de profil"
msgctxt "model"

msgid "retentionRule"
msgstr "Règles de conservation"
msgctxt "model"

msgid "serviceLevel"
msgstr "Niveaux de service"
msgctxt "model"

msgid "archiveRetentionRule"
msgstr "Règle de conservation de l'archive"
msgctxt "model"

msgid "message"
msgstr "Messages"
msgctxt "model"

msgid "certificateOfCompliance"
msgstr "Attestation de conformité"
msgctxt "model"

msgid "certificateOfReception"
msgstr "Certificat de réception"
msgctxt "model"

msgid "certificateOfValidation"
msgstr "Certificat de validation"
msgctxt "model"

msgid "certificateOfDeposit"
msgstr "Certificat de dépôt"
msgctxt "model"

msgid "certificateOfModification"
msgstr "Certificat de modification"
msgctxt "model"

msgid "certificateOfDestruction"
msgstr "Certificat de destruction"
msgctxt "model"

msgid "certificateOfRestitution"
msgstr "Certificat de restitution"
msgctxt "model"

msgid "certificateOfProfileCreation"
msgstr "Certificat de création de profil"
msgctxt "model"

msgid "certificateOfProfileModification"
msgstr "Certificat de modification de profil"
msgctxt "model"

msgid "certificateOfProfileDestruction"
msgstr "Certificat de destruction de profil"
msgctxt "model"

msgid "recordsManagement/archive"
msgstr "Archive"

msgid "Archive information"
msgstr "Information de l'archive"

msgid "Document information :"
msgstr "Information du document :"

msgid "Description metadata"
msgstr "Métadonnées descriptives"

msgid "File extension"
msgstr "Extension du fichier"

msgid "Life cycle events"
msgstr "Évènements du cycle de vie"

msgid "Archive identifier"
msgstr "Identifiant de l'archive"

msgid "Originator archive identifier"
msgstr "Identifiant"

msgid "Archiver archive identifier"
msgstr "Cote du Service d'Archives"

msgid "Archive name"
msgstr "Nom"

msgid "Archive address"
msgstr "Adresse de stockage"

msgid "Description class"
msgstr "Classe de description"

msgid "Profile"
msgstr "Profil"

msgid "Agreement reference"
msgstr "Convention d'archivage"

msgid "Service level"
msgstr "Niveau de service"

msgid "Description level"
msgstr "Niveau de description"

msgid "digitalResourceId"
msgstr "ID de ressource électronique"

msgid "archiver"
msgstr "Service d'Archives"
msgctxt "org"

msgid "depositor"
msgstr "Service Versant"
msgctxt "org"

msgid "originator"
msgstr "Service Producteur"
msgctxt "org"

msgid "Originator"
msgstr "Service Producteur"

msgid "Archiver"
msgstr "Service d'Archives"

msgid "Depositor"
msgstr "Service Versant"

msgid "Access rule code"
msgstr "Code de communicabilité"

msgid "Communication date"
msgstr "Date de communicabilité"

msgid "File name"
msgstr "Nom de fichier"

msgid "Indexing / keyword"
msgstr "Indexation / Mot-clé"

msgid "Integrity"
msgstr "Intégrité"

msgid "Size (octets)"
msgstr "Taille (octets)"

msgid "preservation"
msgstr "Conservation"

msgid "Access entries"
msgstr "Accès"

msgid "Read only"
msgstr "Lecture seule"

msgid "Deposit date"
msgstr "Date de dépôt"

msgid "Last check date"
msgstr "Date du dernier contrôle"

msgid "Date of deposit from"
msgstr "Date de dépôt du"

msgid "to"
msgstr "au"
msgctxt "date"

msgid "Language"
msgstr "Langage"

msgid "Edit archive"
msgstr "Modification d'archive"

msgid "Unfreeze"
msgstr "Dégeler"

msgid "Freeze"
msgstr "Geler"

msgid "Change disposal rule"
msgstr "Modifier la règle de conservation"

msgid "Restore profile rule"
msgstr "Restaurer la règle du profil"

msgid "Specific rule"
msgstr "Règle spécifique"

msgid "Remove the specific rule"
msgstr "Retirer la règle"

msgid "Day(s)"
msgstr "Jour(s)"

msgid "Month(s)"
msgstr "Mois"

msgid "Year(s)"
msgstr "An(s)"

msgid "Retention rule"
msgstr "Règle de conservation"

msgid "Start date"
msgstr "Date de départ du calcul"

msgid "Duration"
msgstr "Durée"

msgid "Code"
msgstr "Nom de la règle"
msgctxt "archiveInformation"

msgid "Final disposition"
msgstr "Sort final"

msgid "Disposal date"
msgstr "Date d'élimination"

msgid "Originator identifier"
msgstr "Identifiant producteur"

msgid "Originator req number"
msgstr "Identifiant Service Producteur"

msgid "Frozen"
msgstr "Gelée"

msgid "Disposed"
msgstr "Éliminée"

msgid "Preserved"
msgstr "Conservée"

msgid "Error"
msgstr "Erreur"

msgid "error"
msgstr "Erreur"

msgid "Waiting for restitution"
msgstr "En cours de restitution"

msgid "Waiting for destruction"
msgstr "En cours d'élimination"

msgid "Restituted"
msgstr "Restituée"

msgid "Request of communication"
msgstr "Demander la communication"

msgid "Flag for restitution"
msgstr "Demander la restitution"

msgid "Validate restitution"
msgstr "Valider la restitution"

msgid "Cancel restitution request"
msgstr "Annuler la demande de restitution"

msgid "Flag for destruction"
msgstr "Demander l'élimination"

msgid "Cancel destruction request"
msgstr "Annuler la demande d'élimination"

msgid "Flag for transfer"
msgstr "Demander le transfert"

msgid "Verify integrity"
msgstr "Vérifier l'intégrité"

msgid "Request a modification"
msgstr "Demander une modification"

msgid "Edit access rule"
msgstr "Modifier la règle de communicabilité"

msgid "You not have the permission to view the documents of archive"
msgstr "Vous n'avez pas la permission de visualiser les documents de cette archive"

msgid "You not have the permission to view the informations of archive"
msgstr "Vous n'avez pas la permission de visualiser les informations de cette archive"

msgid "Archival profile"
msgstr "Profil d'archive"

msgid "Retention start date"
msgstr "Date de référence"

msgid "Retention duration"
msgstr "Durée de conservation"

msgid "Certificate of deposit"
msgstr "Attestation de dépôt"

msgid "Certificate of compliance"
msgstr "Attestation de conformité"

msgid "Certificate of deletion"
msgstr "Attestation de suppression"

msgid "Archival agreement"
msgstr "Accord de versement"

msgid "Search archives"
msgstr "Gestion"

msgid "Archive identifier of originator"
msgstr "Identifiant d'archive producteur"

msgid "Expired"
msgstr "Échue"

msgid "Not expired"
msgstr "Non échue"

msgid "DAU"
msgstr "DUA"

msgid "result(s)"
msgstr "résultat(s)"

msgid "archive(s) on"
msgstr "archive(s) sur"

msgid "Search"
msgstr "Rechercher"

msgid "Reset"
msgstr "Réinitialiser"

msgid "From date"
msgstr "Date de début"

msgid "To date"
msgstr "Date de fin"

msgid "frozen"
msgstr "Gelée"

msgid "disposable"
msgstr "En cours d'élimination"

msgid "Select an archiver"
msgstr "Sélectionnez un Service d'Archives"

msgid "transferable"
msgstr "En cours de transfert"

msgid "transfered"
msgstr "Transférée"

msgid "disposed"
msgstr "Éliminée"

msgid "restituted"
msgstr "Restituée"

msgid "restituable"
msgstr "En cours de restitution"

msgid "preserved"
msgstr "Conservée"

msgid "destruction"
msgstr "Destruction"

msgid "Preservation"
msgstr "Conservation"

msgid "Disposable archives"
msgstr "Archives à terme"

msgid "disposable archive(s)"
msgstr "archive(s) à terme"

msgid "Disposable archives only"
msgstr "Archives expirées seulement"

msgid "Set for disposal"
msgstr "Marquer pour élimination"

msgid "Print"
msgstr "Imprimer"

msgid "Timestamp :"
msgstr "Horodatage :"

msgid "Archive identifier :"
msgstr "Identifiant de l'archive :"

msgid "Archiver name :"
msgstr "Nom du service d'archive :"

msgid "Originator name:"
msgstr "Nom du service producteur :"

msgid "Hash"
msgstr "Empreinte numérique"

msgid "Archival agency :"
msgstr "Service d'Archives :"

msgid "Legal classification :"
msgstr "Forme juridique :"

msgid "Tax identifier :"
msgstr "Numéro de TVA intraco :"

msgid "Registration number :"
msgstr "SIREN / SIRET :"

msgid "Requesting agency :"
msgstr "Service demandeur :"

msgid "Depositor agency :"
msgstr "Service Versant :"

msgid "Event type :"
msgstr "Type d'évènement :"

msgid "User :"
msgstr "Utilisateur :"

msgid "Mail :"
msgstr "Courriel :"

msgid "Telephone number :"
msgstr "Numéro de téléphone :"

msgid "Requesting organization :"
msgstr "Organisation demandeur :"

msgid "Life cycle"
msgstr "Cycle de vie"

msgid "Archival agency"
msgstr "Service d'Archives"

msgid "Requesting agency"
msgstr "Service demandeur"

msgid "Access rule duration"
msgstr "Durée de la règle"

msgid "year(s)"
msgstr "an(s)"

msgid "month(s)"
msgstr "mois"

msgid "day(s)"
msgstr "jour(s)"

msgid "Comment"
msgstr "Commentaire"

msgid "Archival status set to 'Dispose'."
msgstr "Le statut de l'archive a été changé en 'à éliminer'."

msgid "The archive is frozen."
msgstr "L'archive a été gelée."

msgid "The archive is unfrozen."
msgstr "L'archive a été dégelée."

msgid "Archival status updated."
msgstr "Le statut de l'archive a été changé."

msgid "Retention rule updated."
msgstr "La règle de conservation a été modifiée."

msgid "Disposal rule"
msgstr "Règle de conservation"

msgid "Disposal rule origin"
msgstr "Règle appliquée"

msgid "Archive Retention rule updated"
msgstr "Règle de conservation de l'archive modifiée"

msgid "Certificate of reception created"
msgstr "Certificat de réception créé"

msgid "Certificate of validation created"
msgstr "Certificat de validation créé"

msgid "Certificate of modification created"
msgstr "Certificat de modification créé"

msgid "Certificate of profile modification created"
msgstr "Certificat de modification de profil créé"

msgid "Archive received"
msgstr "Archive reçue"

msgid "Archive validation succeeded"
msgstr "Archive validée avec succès"

msgid "Archive created"
msgstr "Archive créée"

msgid "Access Code modification"
msgstr "Règle de communicabilité modifiée"

msgid "Retention rule updated"
msgstr "Règle de conservation modifiée"

msgid "Archive integrities"
msgstr "Intégrité d'archive(s)"

msgid "archives check"
msgstr "archive(s) sélectionnée(s)"

msgid "Result"
msgstr "Résultat"

msgid "archive(s) on"
msgstr "archive(s) sur"

msgid "Select all"
msgstr "Tous"

msgid "For restitution"
msgstr "Pour restitution"

msgid "For destruction"
msgstr "Pour destruction"

msgid "Resource information"
msgstr "Informations de la ressource"

msgid "Hash algorithm"
msgstr "Algorithme d'empreinte"

msgid "Size"
msgstr "Taille"

msgid "Addresses"
msgstr "Adresses"

msgid "Show the archive info"
msgstr "Afficher les informations de l'archive"

msgid "Archiver Name"
msgstr "Service d'Archives"

msgid "Depositor Name"
msgstr "Service Versant"

msgid "Originator Name"
msgstr "Service Producteur"

msgid "bits"
msgstr "octets"

msgid "Identifier"
msgstr "Identifiant"

msgid "Document type"
msgstr "Type de document"

msgid "No archives selected."
msgstr "Pas d'archives sélectionnées."

msgid "This action is not relevant for the selection."
msgstr "Cette action n'est pas compatible avec les archives sélectionnées."

msgid "This action is not relevant for this archive."
msgstr "Cette action n'est pas compatible avec cette archive."

msgid "Cancel destruction"
msgstr "Annuler la destruction"

msgid "archives selected."
msgstr "archives sélectionnées."
msgctxt "validationModal"

msgid "Relationships"
msgstr "Relations"

msgid "Related archive"
msgstr "Archive liée"

msgid "Information"
msgstr "Informations"

msgid "History"
msgstr "Historique"

msgid "Modify retention rule"
msgstr "Modifier la règle de conservation"

msgid "Modify access rule"
msgstr "Modifier la règle de communicabilité"

msgid "Access rule"
msgstr "Règle de communicabilité"

msgid "The modification will be applied on"
msgstr "L'action portera sur"
msgctxt "validationModal"

msgid "%1$s archive(s) flagged for restitution."
msgstr "%1$s archive(s) marquée(s) pour restitution."

msgid "%1$s / %3$s archive(s) flagged for destruction."
msgstr "%1$s archive(s) marquée(s) pour destruction."

msgid "%2$s / %3$s archive(s) not eliminable."
msgstr "%2$s archive(s) non éliminable(s)."

msgid "The archive is flagged for destruction."
msgstr "L'archive est marquée pour la destruction."

msgid "The archive cannot be eliminated."
msgstr "L'archive n'est pas éliminable."

msgid "%1$s archive(s) flagged for conversion."
msgstr "%1$s archive(s) marquée(s) pour conversion."

msgid "%1$s document(s) converted."
msgstr "%1$s document(s) converti(s)."

msgid "%1$s archive(s) modified."
msgstr "%1$s archive(s) modifiée(s)."

msgid "%1$s archive(s) freezed."
msgstr "%1$s archive(s) gelée(s)."

msgid "%1$s archive(s) unfreezed."
msgstr "%1$s archive(s) dégelée(s)."

msgid "%1$s transfer(s) validated."
msgstr "%1$s transfert(s) validé(s)."

msgid "%1$s transfer(s) can not be validate(s)."
msgstr "%1$s transfert(s) ne sont pas validé(s)."

msgid "%1$s restitution(s) validated."
msgstr "%1$s restitution(s) validée(s)."

msgid "%1$s restitution(s) canceled."
msgstr "%1$s restitution(s) annulée(s)."

msgid "%1$s destruction(s) canceled."
msgstr "%1$s destruction(s) annulée(s)."

msgid "%1$s archive(s) can not be flagged because an action is already in progress on this(these) archive(s) (or on one of its/their objects)."
msgstr "%1$s archive(s) n'est(ne sont) pas marquée(s) car une action est déjà en cours sur cette(ces) archive(s) (ou sur l'un de leurs objets)."

msgid "%1$s archive(s) can not be modified."
msgstr "%1$s archive(s) ne sont pas modifiée(s)."

msgid "%1$s archive(s) can not be freezed."
msgstr "%1$s archive(s) ne sont pas gelée(s)."

msgid "%1$s archive(s) can not be unfreezed."
msgstr "%1$s archive(s) ne sont pas dégelée(s)."

msgid "%1$s restitution(s) can not be validate(s)."
msgstr "%1$s restitution(s) ne sont pas validée(s)."

msgid "%1$s restitution(s) can not be canceled."
msgstr "%1$s restitution(s) ne sont pas annulée(s)."

msgid "%1$s destruction(s) can not be canceled."
msgstr "%1$s destruction(s) ne sont pas annulée(s)."

msgid "%1$s resource(s) can\'t be deleted."
msgstr "la ressource %1$s ne peut pas être supprimée."

msgid "%1$s resource(s) deleted."
msgstr "%1$s ressource(s) supprimée(s)"

msgid "Restitution message created"
msgstr "Message de restitution créé."

msgid "The archive has been deposited"
msgstr "L'archive a été déposée"

msgid "No organization unit"
msgstr "Pas d'unité organisationnelle"

msgid "You have to choose a working organization unit to proceed this action."
msgstr "Vous devez choisir une unité organisationnelle de travail pour effectuer cette action."

msgid "Permission denied: You have to choose a working organization unit to proceed this action."
msgstr "Permission non accordée : vous devez choisir une unité organisationnelle de travail pour effectuer cette action."

msgid "The description class does not match with the archival profile."
msgstr "La classe description ne correspond pas aux profils d'archive."

msgid "The processing status isn't initial"
msgstr "Le statut de traitement choisi n'est pas de type initial"

msgid "Archive not set for destruction."
msgstr "L'archive n'est pas prévue pour être détruite."

msgid "Children archives : Archive not set for destruction."
msgstr "Une sous-archive n'est pas prévue pour être détruite."

msgid "Disposal date not reached."
msgstr "DUA non échue."

msgid "Children archives : Disposal date not reached."
msgstr "La DUA d'une des sous-archives n'est pas échue."

msgid "Children archives : Final disposition must be advised for this action"
msgstr "Le sort final d'une des sous-archives n'est pas renseigné."

msgid "There is a missing management information (date or retention rule)."
msgstr "Il manque une information concernant la gestion de la conservation de l'archive (date et/ou règle de conservation)."

msgid "Children archives : There is a missing management information (date or retention rule)."
msgstr "Il manque une information concernant la gestion de la conservation d'une des sous-archives (date et/ou règle de conservation)."

msgid "Final disposition must be advised for this action"
msgstr "Un sort final doit être renseigné pour exécuter cette action."

msgid "Profil and data management not found"
msgstr "Profil et données de gestion non trouvés"

msgid "Representation information"
msgstr "Information de représentation"

msgid "Preservation Description Information"
msgstr "Information de pérennisation"

msgid "Content data object"
msgstr "Contenu de données"

msgid "isConversionOf"
msgstr "Conversion"

msgid "The communication request has been send, check your requests list to follow the treatment"
msgstr "Demande de communication envoyée, consultez votre liste de demandes pour en suivre le traitement"

msgid "The communication request has been accepted, check your requests list to get the archived package"
msgstr "Demande de communication acceptée, consultez votre liste de demandes pour obtenir le paquet archivé"

msgid "The communication request is in derogation, check your requests list to follow the treatment"
msgstr "Demande de communication en dérogation, consultez votre liste de demandes pour en suivre le traitement"

msgid "Invalid action for this archive"
msgstr "Action non valide pour cette archive"

msgid "Parent archive"
msgstr "Dossier"

msgid "Children archives"
msgstr "Objets contenus"

msgid "A keyword label is missing."
msgstr "Un libellé de mot-clé n'a pas été renseigné."

msgid "The indexes of the archive have been modified"
msgstr "Les index de l'archive ont été modifiés"

msgid "isTimestampOf"
msgstr "Jeton d'horodatage"
msgctxt "relationship"

msgid "Indexing modification"
msgstr "Modification de l'index"

msgid "Reference"
msgstr "Cote"

msgid "Reference"
msgstr "Référence"
msgctxt "message"

msgid "Select a rule"
msgstr "Sélectionnez une règle"


msgid "Acces rule duration"
msgstr "Durée de la règle"

msgid "Size (bytes)"
msgstr "Taille (octets)"

msgid "Creation date"
msgstr "Date de création"

msgid "Management information"
msgstr "Information de gestion"

msgid "Management metadata"
msgstr "Métadonnées de gestion"

msgid "Descriptive metadata"
msgstr "Métadonnées descriptives"

msgid "Metadata"
msgstr "Métadonnées"

msgid "Import"
msgstr "Importer"

msgid "Import with validation"
msgstr "Importer avec validation"

msgid "Click or drop a document"
msgstr "Cliquer ou déposer un document"

msgid "Click or drop documents"
msgstr "Cliquez ou déposez des documents"

msgid "Folder"
msgstr "Dossier"

msgid "Create"
msgstr "Créer"

msgid "File plan position"
msgstr "Position"

msgid "Empty folder"
msgstr "Le dossier est vide"

msgid "%1$s after %2$s"
msgstr "%1$s après %2$s"

msgid "bytes"
msgstr "octets"

msgid "New archive"
msgstr "Nouvelle archive"

msgid "Archive updated"
msgstr "Archive modifiée"

msgid "Add field"
msgstr "Ajouter un champ"

msgid "Missing Label"
msgstr "Un libellé est nécessaire avec la valeur"

msgid "Label"
msgstr "Libellé"

msgid "Text"
msgstr "Texte"

msgid "Select a retention rule"
msgstr "Sélectionner une règle de conservation"

msgid "No result"
msgstr "Aucun résultat"

msgid "Sub-archive of"
msgstr "Sous-archive de"

msgid "Sub-archive"
msgstr "Sous-archive"

msgid "Zip container"
msgstr "Fichier conteneur"

msgid "Originating date"
msgstr "Date du document"

msgid "Today's date"
msgstr "Date du jour"

msgid "Text indexation"
msgstr "Indexation du texte"

msgid "Profiles"
msgstr "Profils"

msgid "Partial retention rule"
msgstr "Règle de conservation partielle"

msgid "Fulltext"
msgstr "Texte intégral"

msgid "To be defined later"
msgstr "À définir plus tard"

msgid "Producer"
msgstr "Service producteur"

msgid "Organization"
msgstr "Organisation"

msgid "No originator found"
msgstr "Aucun producteur trouvé"

msgid "No archival agency found"
msgstr "Aucun Service d'Archives trouvé"

msgid "Message"
msgstr "Bordereau(x)"

msgid "ArchiveTransfer"
msgstr "Transfert d'archive"

msgid "ArchiveModificationNotification"
msgstr "Notification de modification"

msgid "ArchiveDeliveryRequest"
msgstr "Demande de communication "

msgid "ArchiveRestitutionRequest"
msgstr "Demande de restitution"

msgid "ArchiveDestructionRequest"
msgstr "Demande d'élimination"

msgid "ArchiveModificationRequest"
msgstr "Demande de modification"

msgid "Rule name"
msgstr "Nom de la règle"

msgid "DUA Due date"
msgstr "Date d'échéance de la DUA"

msgid "No working organization"
msgstr "Aucune organisation sélectionnée"

msgid "You have to choose a working organization unit to proceed this action."
msgstr "L'utilisateur courant n'est affecté à aucune organisation. Contactez votre administrateur."

msgid "Code"
msgstr "Nom de la règle"
msgctxt "archiveInformation"

msgid "Change start date"
msgstr "Changer la date"

msgid "Archive Transfer"
msgstr "Demander le transfert sortant"

msgid "Invalid object"
msgstr "Objet invalide"

msgid "Attempt to modify readonly field(s)"
msgstr "Tentative de modification d'une donnée en lecture seule."

msgid "Do not update the final disposition"
msgstr "Ne pas mettre à jour le sort final"

msgid "Edit metadata"
msgstr "Modifier les métadonnées"

msgid "Unlimited"
msgstr "Illimitée"

msgid "Messages"
msgstr "Bordereaux"

msgid "Add sub-archive"
msgstr "Ajout de sous-archive"

msgid "archive found"
msgstr "archive trouvée"

msgid "archives found"
msgstr "archives trouvées"

msgid "Results of search in"
msgstr "Résultats de la recherche dans"

msgid "Fileplan level"
msgstr "Niveau dans le plan"

msgid "Item"
msgstr "Pièce"

msgid "File"
msgstr "Dossier"

msgid "Processing statuses"
msgstr "Statuts du flux de travail"

msgid "Processing status"
msgstr "Statut initial du flux de travail"

msgid "Last modification date"
msgstr "Date de mise à jour"

msgid "Add a resource"
msgstr "Ajouter une ressource"

msgid "Delete the resource"
msgstr "Supprimer la ressource"

msgid "Cancel"
msgstr "Annuler"

msgid "Import a resource"
msgstr "Ajout d'une ressource"

msgid "Do you really want to delete this resource ?"
msgstr "Voulez-vous vraiment supprimer cette ressource ?"

msgid "This action will be irreversible."
msgstr "Cette action sera irréversible."

msgid "Modification request sent"
msgstr "Demande de modification envoyée"

msgid "Click to add '%1$s'"
msgstr "Cliquer pour ajouter '%1$s'"

msgid "Click to modify '%1$s'"
msgstr "Cliquer pour modifier '%1$s'"

msgid "* At least one field must be filled in."
msgstr "* Au moins un champ doit être renseigné."

msgid "You must fill all required fields (*)"
msgstr "Vous devez remplir tous les champs requis (*)"

msgid "Export to "
msgstr "Exporter en "

msgid "Full name"
msgstr "Nom Complet"

msgid "First Name"
msgstr "Prénom"

msgid "Birth Name"
msgstr "Nom de naissance"

msgid "Given Name"
msgstr "Nom usuel"

msgid "Gender"
msgstr "Genre"

msgid "Birth date"
msgstr "Date de naissance"

msgid "Birth place"
msgstr "Lieu de naissance"

msgid "Death date"
msgstr "Date de décès"

msgid "Death place"
msgstr "Lieu du décès"

msgid "Nationality"
msgstr "Nationalité(s)"

msgid "Identifier"
msgstr "Identifiant"

msgid "Corporation name"
msgstr "Nom de l'entreprise"

msgid "Mandate"
msgstr "Mandat"

msgid "Function"
msgstr "Fonction"

msgid "Geographic name"
msgstr "Nom du lieu"

msgid "Address"
msgstr "Adresse"

msgid "Postal Code"
msgstr "Code postal"

msgid "City"
msgstr "Ville"

msgid "Region"
msgstr "Région"

msgid "Country"
msgstr "Pays"

msgid "Activity"
msgstr "Activité"

msgid "Archival Agency ArchiveUnit Identifier"
msgstr "Identifiant d'archive"

msgid "Originating Agency ArchiveUnit Identifier"
msgstr "Identifiant métier"

msgid "Transferring Agency ArchiveUnit Identifier"
msgstr "Identifiant de versement"

msgid "Custodial History"
msgstr "Historique de conservation"

msgid "OAIS Type"
msgstr "Type OAIS"

msgid "Document Type"
msgstr "Type de document"

msgid "Language"
msgstr "Langue"

msgid "Status"
msgstr "Statut"

msgid "Version"
msgstr "Version"

msgid "Keyword"
msgstr "Mots-clés"

msgid "Tag"
msgstr "Tags"

msgid "Coverage"
msgstr "Couverture"

msgid "OriginatingAgency"
msgstr "Service producteur"

msgid "SubmissionAgency"
msgstr "Service versant"

msgid "Authorized Agent"
msgstr "Services autorisés"

msgid "Writer"
msgstr "Auteur(s)"

msgid "Addressee"
msgstr "Destinataire(s)"

msgid "Recipient"
msgstr "En copie"

msgid "Transmitter"
msgstr "Émetteur"

msgid "Sender"
msgstr "Expéditeur"

msgid "Created date"
msgstr "Date de création"

msgid "Transacted date"
msgstr "Date de transaction"

msgid "Acquired date"
msgstr "Date d'acquisition"

msgid "Sent date"
msgstr "Date d'envoi"

msgid "Received date"
msgstr "Date de réception"

msgid "Registered date"
msgstr "Date d'enregistrement"

msgid "Start date"
msgstr "Date de début"

msgid "End date"
msgstr "Date de fin"

msgid "Event"
msgstr "Événements de traitement"

msgid "Fileplan position"
msgstr "Position dans le plan de classement"

msgid "Title"
msgstr "Intitulé"

msgid "Description Level"
msgstr "Niveau de description"

msgid "Originating System Id"
msgstr "Identifiant d'origine"

msgid "Gps Version"
msgstr "Version du GPS"

msgid "Altitude reference"
msgstr "Référentiel d'altitude"

msgid "Latitude Reference"
msgstr "Référentiel de latitude"

msgid "Longitude Reference"
msgstr "Référentiel de longitude"

msgid "DateStamp"
msgstr "Heure et date de la position gps"

msgid "Signer"
msgstr "Signataire(s)"

msgid "Validator"
msgstr "Validateur(s)"

msgid "ReferencedObject"
msgstr "Référence à l'objet signé"

msgid "Signing time"
msgstr "Date et heure de la signature"

msgid "Validation time"
msgstr "Date et heure de la validation"

msgid "Masterdata"
msgstr "Référentiel des personnes et organisations"

msgid "Object"
msgstr "Objet"
