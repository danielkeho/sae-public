FROM debian:9.8

LABEL authors="Alexandre Morin <alexandre.morin@maarch.org>"

ENV POSTGRES_DB="maarchRMAP"
ENV POSTGRES_USER="maarch"
ENV POSTGRES_PASSWORD="maarch"

ENV APP_VERSION="master"
ENV POSTGRES_VERSION="9.6"

RUN apt-get update -y && apt-get upgrade -y && apt-get install -y \
    apache2 \
    php \
    php-pgsql \
    php-xml \
    php-cli \
    php-gd \
    php-opcache \
    php-common \
    php-mbstring \
    openssl \
    default-jre \
    git \
    p7zip-full \
    postgresql \
    postgresql-client \
    libapache2-mod-php \
    libreoffice-writer \
    wkhtmltopdf \
    xvfb \
    && apt-get clean

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen

ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR
ENV LC_ALL fr_FR.UTF-8

WORKDIR /var/www/
RUN git clone --depth 1 -b  $APP_VERSION https://gitlab.com/danielkeho/sae-public.git laabs \
    && cd laabs/src/ext \
    && git clone --depth 1 -b $APP_VERSION https://labs.maarch.org/maarch/archivesPubliques.git  \
    && cp archivesPubliques/data/conf/vhost.conf.default archivesPubliques/data/conf/vhost.conf \
    && cp archivesPubliques/data/conf/configuration.ini.default archivesPubliques/data/conf/configuration.ini \
    && cp archivesPubliques/data/conf/confvars.ini.default archivesPubliques/data/conf/confvars.ini \
    && useradd -m -g www-data maarch \
    && export APACHE_RUN_USER=maarch \
    && a2enmod rewrite \
    && a2enmod env \
    && mkdir -p /var/www/laabs/data/maarchRM/repository/archives_1 /var/www/laabs/data/maarchRM/repository/archives_2 \
    && mkdir -p /var/www/laabs/data/maarchRM/tmp/ \
    && chown -R www-data:www-data /var/www/ \
    && chmod -R 777 /var/www/

USER postgres

RUN /etc/init.d/postgresql start \
    && psql --command "CREATE USER $POSTGRES_USER;" \
    && psql --command "ALTER ROLE $POSTGRES_USER WITH CREATEDB;" \
    && psql --command "ALTER ROLE $POSTGRES_USER WITH SUPERUSER;" \
    && psql --command "ALTER USER $POSTGRES_USER WITH ENCRYPTED PASSWORD '$POSTGRES_PASSWORD';" \
    && psql --command "CREATE DATABASE \"$POSTGRES_DB\" WITH OWNER $POSTGRES_USER;" \
    && echo "host all  all    127.0.0.1/32  trust" >> /etc/postgresql/$POSTGRES_VERSION/main/pg_hba.conf \
    && export PGPASSWORD=$POSTGRES_PASSWORD \
    && /var/www/laabs/src/ext/archivesPubliques/data/batch/pgsql/schema.sh \
    && /var/www/laabs/src/ext/archivesPubliques/data/batch/pgsql/data.sh

EXPOSE 80

USER root

WORKDIR /etc/apache2/sites-available/
RUN touch maarchRM.conf \
    && echo "Include /var/www/laabs/src/ext/archivesPubliques/data/conf/vhost.conf" >> maarchRM.conf \
    && a2ensite maarchRM.conf \
    && a2dissite 000-default.conf

RUN touch /var/www/run.sh \
    && echo "service postgresql start" >> /var/www/run.sh \
    && echo "service cron start" >> /var/www/run.sh \
    && echo "#!/bin/bash\nxvfb-run -a --server-args=\"-screen 0, 1024x768x24\" /usr/bin/wkhtmltopdf -q \$*" > /usr/bin/newwkhtmltopdf.sh \
    && echo "chmod a+x /usr/bin/newwkhtmltopdf.sh" >> /var/www/run.sh \
    && echo "ln -s /usr/bin/newwkhtmltopdf.sh /usr/local/bin/wkhtmltopdf" >> /var/www/run.sh \
    && chmod +x /var/www/run.sh \
    && echo "* * * * * maarch /var/www/laabs/src/ext/archivesPubliques/data/batch/scheduling.sh" >> /etc/crontab \
    && sed -i -e 's!ABjHAkI20y/RfLQ6YqkFhI4YnGDQ+ITnofYu5TFmNIVzuVb5Yqhf1Hdk879TUAewqrEMXjPwIkQ=!phdF9WkJuTKkDuPXoqDZuOjLMAFGC6Zru9T2pCmR5ZzEVAKD8OVzduADSaonEtc2kUSobb1phMg=!' /var/www/laabs/src/ext/archivesPubliques/data/batch/0-token.txt \
    && echo "/usr/sbin/apache2ctl -D FOREGROUND" >> /var/www/run.sh

CMD /var/www/run.sh
